package db

import (
	"fmt"
	"os"
)

type leafFile struct {
	f *os.File
}

func openFile(name string) (leafFile, error) {
	f, err := os.OpenFile(name, os.O_CREATE|os.O_RDWR, 0644)

	if err != nil {
		return leafFile{}, err
	}
	stat, err := f.Stat()
	if err != nil {
		f.Close()
		return leafFile{}, err
	}
	byteSize := stat.Size()
	if byteSize < 0 {
		panic(fmt.Sprintf("internal error, file size %d < 0", byteSize))
	}

	if leftOver := byteSize % LeafSize; leftOver != 0 {
		fmt.Fprintf(os.Stderr, "ignoring partial leaf, %d bytes, at end of file", leftOver)
	}
	return leafFile{f}, nil
}

func (f *leafFile) close() {
	if f != nil && f.f != nil {
		f.f.Close()
		f.f = nil
	}
}

func (f *leafFile) read(index int64, leafBlobs []LeafBlob) (int, error) {
	if index < 0 {
		return 0, fmt.Errorf("invalid argument index %d (must be >= 0)",
			index)
	}
	for i := 0; i < len(leafBlobs); i++ {
		if n, err := f.f.ReadAt(leafBlobs[i][:], (index+int64(i))*LeafSize); n != LeafSize {
			return i, err
		}
	}
	return len(leafBlobs), nil
}

func (f *leafFile) write(index int64, leafBlobs []LeafBlob) (int, error) {
	for i, b := range leafBlobs {
		if n, err := f.f.WriteAt(b[:], (index+int64(i))*LeafSize); n != LeafSize {
			return i, err
		}
	}
	return len(leafBlobs), nil
}
