package db

import (
	"fmt"
	"math/bits"

	"sigsum.org/sigsum-go/pkg/crypto"
	"sigsum.org/sigsum-go/pkg/merkle"
)

type state struct {
	leafs []crypto.Hash
	// Maps leaf hash to index.
	leafIndex map[crypto.Hash]int
	// Stack with hash of one power-of-two subtree per one-bit in
	// current size.
	stack []crypto.Hash
}

func (s *state) size() uint64 {
	if len(s.leafs) != len(s.leafIndex) {
		panic(fmt.Errorf("invalid state: %d leafs, %d index entries",
			len(s.leafs), len(s.leafIndex)))
	}
	return uint64(len(s.leafs))
}

func (s *state) isPresent(leafHash *crypto.Hash) bool {
	_, ok := s.leafIndex[*leafHash]
	return ok
}

// Returns true if added, false for duplicates.
func (s *state) addLeafHash(leafHash *crypto.Hash) bool {
	if _, ok := s.leafIndex[*leafHash]; ok {
		return false
	}
	h := *leafHash
	s.leafIndex[h] = len(s.leafs)
	s.leafs = append(s.leafs, h)

	for i := len(s.leafs); (i & 1) == 0; i >>= 1 {
		h = merkle.HashInteriorNode(&s.stack[len(s.stack)-1], &h)
		s.stack = s.stack[:len(s.stack)-1]
	}
	s.stack = append(s.stack, h)
	if popc := bits.OnesCount(uint(len(s.leafs))); popc != len(s.stack) {
		panic(fmt.Errorf("internal error: popc %d, len 0x%x", popc, len(s.stack)))
	}

	return true
}

func hashStack(stack []crypto.Hash) crypto.Hash {
	if len(stack) == 0 {
		panic(fmt.Errorf("internal error, empty stack"))
	}
	h := stack[len(stack)-1]
	for i := len(stack) - 1; i > 0; i-- {
		h = merkle.HashInteriorNode(&stack[i-1], &h)
	}
	return h
}

func (s *state) getRootHash() crypto.Hash {
	if len(s.stack) == 0 {
		// Special case, all-zero hash
		return crypto.Hash{}
	}
	return hashStack(s.stack)
}

func rootOf(leaves []crypto.Hash) crypto.Hash {
	if len(leaves) == 0 {
		panic(fmt.Errorf("internal error"))
	}
	state := state{leafIndex: make(map[crypto.Hash]int)}
	for _, leaf := range leaves {
		if !state.addLeafHash(&leaf) {
			panic(fmt.Errorf("internal error, unexpected duplicate"))
		}
	}
	return state.getRootHash()
}

func reversePath(p []crypto.Hash) []crypto.Hash {
	n := len(p)
	for i := 0; i < n-1-i; i++ {
		p[i], p[n-1-i] = p[n-1-i], p[i]
	}
	return p
}

// Produces inclusion path from root down (opposite to rfc 9162 order).
// Stack and size represent the larger tree, where leaves is a prefix.
func inclusion(leaves []crypto.Hash, m uint64, stack []crypto.Hash, size uint64) []crypto.Hash {
	p := []crypto.Hash{}

	// Try reusing hashes of internal nodes on the stack; useful
	// if m and len(leaves) are close to the end of the tree.
	for len(leaves) > 1 && len(stack) > 1 {
		// Size of subtree represented by stack[0]
		k := uint64(1) << (bits.Len64(size-1) - 1)
		if m < k {
			// Could possibly use some other elements of
			// stack, but it gets complicated.
			break
		}
		// k gives a valid split also for the subtree
		// for which we prove inclusion.
		p = append(p, stack[0])
		stack = stack[1:]
		size -= k
		leaves = leaves[k:]
		m -= k
	}

	for len(leaves) > 1 {
		n := uint64(len(leaves))
		k := uint64(1) << (bits.Len64(n-1) - 1)

		// We select the subtree which m is in, for further
		// processing, after adding the hash of the other
		// subtree to the path.
		if m < k {
			p = append(p, rootOf(leaves[k:]))
			leaves = leaves[:k]
		} else {
			p = append(p, rootOf(leaves[:k]))
			leaves = leaves[k:]
			m -= k
		}
	}
	return p
}

func (s *state) proveInclusion(index, size uint64) []crypto.Hash {
	if index >= size || size > s.size() {
		panic(fmt.Errorf("invalid argument index %d, size %d, tree %d", index, size, s.size()))
	}
	return reversePath(inclusion(s.leafs[:size], index, s.stack, s.size()))
}

// Based on RFC 9161, 2.1.4.1, but produces path in opposite order.
func consistency(leaves []crypto.Hash, m uint64, stack []crypto.Hash, size uint64) []crypto.Hash {
	p := []crypto.Hash{}
	complete := true

	// Try reusing hashes of internal nodes on the stack; useful
	// if m and len(leaves) are close to the end of the tree.
	for len(stack) > 1 {
		n := uint64(len(leaves))
		if m == n {
			break
		}
		// Size of subtree represented by stack[0]
		k := uint64(1) << (bits.Len64(size-1) - 1)
		if m <= k {
			// Could possibly use some other elements of
			// stack, but it gets complicated.
			break
		}
		// k gives a valid split also for the subtree
		// for which we prove consistency.
		p = append(p, stack[0])
		stack = stack[1:]
		size -= k
		leaves = leaves[k:]
		m -= k
		complete = false
	}
	for {
		n := uint64(len(leaves))
		if m > n {
			panic(fmt.Errorf("internal error, m %d, n %d", m, n))
		}
		if m == n {
			if complete {
				return p
			}
			return append(p, rootOf(leaves))
		}
		k := uint64(1) << (bits.Len64(n-1) - 1)
		if m <= k {
			p = append(p, rootOf(leaves[k:]))
			leaves = leaves[:k]
		} else {
			p = append(p, rootOf(leaves[:k]))
			leaves = leaves[k:]
			m -= k
			complete = false
		}
	}
}

func (s *state) proveConsistency(m, n uint64) []crypto.Hash {
	if n == 0 || n > s.size() || m >= n {
		panic(fmt.Errorf("invalid argument m %d, n %d, tree %d", m, n, s.size()))
	}
	return reversePath(consistency(s.leafs[:n], m, s.stack, s.size()))
}
