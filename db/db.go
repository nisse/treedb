package db

import (
	"fmt"
	"io"
	"sigsum.org/sigsum-go/pkg/crypto"
	"sigsum.org/sigsum-go/pkg/merkle"
	"sigsum.org/sigsum-go/pkg/types"
	"time"
)

// Based on log_go/internal/db/client.go
type Client interface {
	AddLeaf(*types.Leaf) bool
	AddSequencedLeaves(leaves []types.Leaf, index uint64) error
	GetTreeHead() types.TreeHead
	// GetConsistencyProof(context.Context, *requests.ConsistencyProof) (*types.ConsistencyProof, error)
	// GetInclusionProof(context.Context, *requests.InclusionProof) (*types.InclusionProof, error)
	// GetLeaves(context.Context, *requests.Leaves) (*[]types.Leaf, error)
}

type db struct {
	leafs []LeafBlob
	state state
}

func (db *db) AddLeafBlob(leaf *LeafBlob) bool {
	leafHash := merkle.HashLeafNode(leaf[:])
	if !db.state.addLeafHash(&leafHash) {
		// Duplicate
		return false
	}
	db.leafs = append(db.leafs, *leaf)
	return true
}

func (db *db) AddLeaf(leaf *types.Leaf) bool {
	var blob LeafBlob
	copy(blob[:], leaf.ToBinary())
	return db.AddLeafBlob(&blob)
}

func (db *db) GetTreeHead() types.TreeHead {
	return types.TreeHead{
		Timestamp: uint64(time.Now().Unix()),
		TreeSize:  uint64(db.state.size()),
		RootHash:  db.state.getRootHash(),
	}
}

func (db *db) AddSequencedLeaves(leaves []types.Leaf, index uint64) error {
	if index != db.state.size() {
		return fmt.Errorf("unexpected index %d, expected %d",
			index, db.state.size())
	}
	// Check for duplicates.
	for i, leaf := range leaves {
		h := merkle.HashLeafNode(leaf.ToBinary())
		if db.state.isPresent(&h) {
			return fmt.Errorf("duplicate leaf for index %d\n",
				uint64(i)+index)
		}
	}
	for _, leaf := range leaves {
		var blob LeafBlob
		copy(blob[:], leaf.ToBinary())
		h := merkle.HashLeafNode(blob[:])
		if !db.state.addLeafHash(&h) {
			panic(fmt.Errorf("duplicate leaf detected too late"))
		}
		db.leafs = append(db.leafs, blob)
	}
	return nil
}

func ReadDb(name string) (Client, error) {
	leafFile, err := openFile(name)
	if err != nil {
		return nil, err
	}
	defer leafFile.close()
	db := db{state: state{leafIndex: make(map[crypto.Hash]int)}}

	for {
		blobs := make([]LeafBlob, 100)
		n, err := leafFile.read(int64(db.state.size()), blobs)
		if n < len(blobs) && err != io.EOF {
			return nil, err
		}
		for i := 0; i < n; i++ {
			if !db.AddLeafBlob(&blobs[i]) {
				return nil, fmt.Errorf("duplicate leaf at index %d", db.state.size())
			}
		}
		if n < len(blobs) {
			break
		}
	}
	return &db, nil
}
