package db

import "sigsum.org/sigsum-go/pkg/crypto"

const LeafSize = 2*crypto.HashSize + crypto.SignatureSize

type LeafBlob [LeafSize]byte
