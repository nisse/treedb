package db

import (
	"testing"

	"encoding/binary"
	"sigsum.org/sigsum-go/pkg/crypto"
	"sigsum.org/sigsum-go/pkg/merkle"
)

func TestGetRootHash(t *testing.T) {
	hashes := newLeaves(5)
	h01 := merkle.HashInteriorNode(&hashes[0], &hashes[1])
	h23 := merkle.HashInteriorNode(&hashes[2], &hashes[3])
	h0123 := merkle.HashInteriorNode(&h01, &h23)

	state := state{leafIndex: make(map[crypto.Hash]int)}
	for i, want := range []crypto.Hash{
		crypto.Hash{},
		hashes[0],
		h01,
		merkle.HashInteriorNode(&h01, &hashes[2]),
		h0123,
		merkle.HashInteriorNode(&h0123, &hashes[4]),
	} {
		if state.size() < uint64(i) {
			if !state.addLeafHash(&hashes[state.size()]) {
				t.Fatalf("addLeaf failed at size %d", state.size())
			}
		}
		if got := state.getRootHash(); got != want {
			t.Errorf("bad root hash for size %d\n  got: %x\n want: %x",
				i, got, want)
		}
	}
}

func TestInclusion(t *testing.T) {
	hashes := newLeaves(5)
	h01 := merkle.HashInteriorNode(&hashes[0], &hashes[1])
	h23 := merkle.HashInteriorNode(&hashes[2], &hashes[3])
	h0123 := merkle.HashInteriorNode(&h01, &h23)

	state := state{leafIndex: make(map[crypto.Hash]int)}
	for _, h := range hashes {
		if !state.addLeafHash(&h) {
			t.Fatalf("addLeaf failed at size %d", state.size())
		}
	}

	// Inclusion path for index i and size 5.
	for i, p := range [][]crypto.Hash{
		[]crypto.Hash{hashes[1], h23, hashes[4]},
		[]crypto.Hash{hashes[0], h23, hashes[4]},
		[]crypto.Hash{hashes[3], h01, hashes[4]},
		[]crypto.Hash{hashes[2], h01, hashes[4]},
		[]crypto.Hash{h0123},
	} {
		if proof := state.proveInclusion(uint64(i), 5); !pathEqual(proof, p) {
			t.Errorf("unexpected inclusion path\n  got: %x\n want: %x\n",
				proof, p)
		}
	}
}

func TestInclusionValid(t *testing.T) {
	hashes := newLeaves(100)

	rootHashes := []crypto.Hash{}
	state := state{leafIndex: make(map[crypto.Hash]int)}
	for _, h := range hashes {
		if !state.addLeafHash(&h) {
			t.Fatalf("addLeaf failed at size %d", state.size())
		}
		rootHashes = append(rootHashes, state.getRootHash())
	}
	for i := 0; i < len(hashes); i++ {
		for n := i + 1; n <= len(hashes); n++ {
			proof := state.proveInclusion(uint64(i), uint64(n))
			if err := merkle.VerifyInclusion(&hashes[i], uint64(i), uint64(n), &rootHashes[n-1], proof); err != nil {
				t.Errorf("inclusion proof not valid, i %d, n %d: %v\n  proof: %x\n",
					i, n, err, proof)
			}
		}
	}
}

func TestConsistency(t *testing.T) {
	hashes := newLeaves(7)
	h01 := merkle.HashInteriorNode(&hashes[0], &hashes[1])
	h23 := merkle.HashInteriorNode(&hashes[2], &hashes[3])
	h0123 := merkle.HashInteriorNode(&h01, &h23)
	h45 := merkle.HashInteriorNode(&hashes[4], &hashes[5])
	h456 := merkle.HashInteriorNode(&h45, &hashes[6])

	state := state{leafIndex: make(map[crypto.Hash]int)}
	for _, h := range hashes {
		if !state.addLeafHash(&h) {
			t.Fatalf("addLeaf failed at size %d", state.size())
		}
	}
	for _, table := range []struct {
		m    uint64
		n    uint64
		path []crypto.Hash
	}{
		{3, 7, []crypto.Hash{hashes[2], hashes[3], h01, h456}},
		{4, 7, []crypto.Hash{h456}},
		{6, 7, []crypto.Hash{h45, hashes[6], h0123}},
	} {
		if proof := state.proveConsistency(table.m, table.n); !pathEqual(proof, table.path) {
			t.Errorf("unexpected inclusion path m %d, n %d\n  got: %x\n want: %x\n",
				table.m, table.n, proof, table.path)
		}
	}
}
func TestConsistencyValid(t *testing.T) {
	hashes := newLeaves(100)

	rootHashes := []crypto.Hash{}
	state := state{leafIndex: make(map[crypto.Hash]int)}
	for _, h := range hashes {
		if !state.addLeafHash(&h) {
			t.Fatalf("addLeaf failed at size %d", state.size())
		}
		rootHashes = append(rootHashes, state.getRootHash())
	}

	for m := 1; m < len(hashes); m++ {
		for n := m + 1; n <= len(hashes); n++ {
			proof := state.proveConsistency(uint64(m), uint64(n))
			if err := merkle.VerifyConsistency(
				uint64(m), uint64(n),
				&rootHashes[m-1], &rootHashes[n-1], proof); err != nil {
				t.Errorf("consistency proof not valid, m %d, n %d: %v\n  proof: %x\n",
					m, n, err, proof)
			}
		}
	}
}

func newLeaves(n int) []crypto.Hash {
	hashes := make([]crypto.Hash, n)
	for i := 0; i < n; i++ {
		var blob LeafBlob
		binary.BigEndian.PutUint64(blob[0:8], uint64(i))
		hashes[i] = merkle.HashLeafNode(blob[:])
	}
	return hashes
}

func pathEqual(a, b []crypto.Hash) bool {
	if len(a) != len(b) {
		return false
	}
	for i, h := range a {
		if h != b[i] {
			return false
		}
	}
	return true
}
